# Description

Ce script crée la structure des entités voulues dans l'arborescence de l'Active Directory, et permet de créer des utilisateurs également. Sont utilisation est assez simple, il suffit de se placer dans un terminal dans le dossier contenant le script, puis de l'invoquer avec ses arguments

```
./creation_ou.ps1 <liste_des_entités> <liste_des_services> {<dn_de_l_entité_parente>}
```

La liste des entités doit être donnée avec des virgules comme séparateurs, la liste des services communs aux entités également s'il n'y a pas différents services et le chemin de l'entité parente est facultatif.

# Exemples

```
./creation_ou.ps1 administration comptabilité,ressources_humaines
```

Créera l'arborescence de l'entité Administration et de ses services comptabilité et ressources humaines à la racine du domaine :

```
dc=chasseneuil,dc=tierslieux86,dc=fr
|-ou=administration
 |-ou=groupes globaux
 | |-cn=employes_comptabilité_administration
 | |-cn=employes_ressources_humaines_administration
 | |-cn=responsables_comptabilité_administration
 | |-cn=responsables_ressources_humaines_administration
 |-ou=ordinateurs
 | |-cn=ordinateur_test
 |-ou=utilisateurs
   |-ou=employes
   | |-ou=comptabilité
   | | |-cn=employe test
   | |-ou=ressources_humaines
   |   |-cn=employe test
   |-ou=responsables
     |-ou=comptabilité
     | |-cn=responsable test
     |-ou=ressources_humaines
       |-cn=responsable test
```

```
./creation_ou.ps1 administration,adherents ou=ou_test,dc=chasseneuil,dc=tierslieux86,dc=fr
```

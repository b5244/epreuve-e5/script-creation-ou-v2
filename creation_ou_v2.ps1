# Ce script permet la création des OU pour le domaine d'un ETP
# Il s'agit de la version 2 faite pour l'arrivée de ValorElec
# Son utilisation est "./creation_ou_v2.ps1 -names nom1,nom2,nom3,... -services service1,service2 -path 'OU=dn,OU=del'emplacement,DC=chasseuneuil,DC=int'"
# Il faut écrire le nom des OU que l'on veut créer séparés par des virgules sans espaces
# Et le chemin ensuite (laisser vide si la création doit se faire à la racine)
# EXEMPLE : ".\creation_ou.ps1 Esporting,3DPrint86 OU=clients_entreprises,DC=chasseuneuil,DC=int"

param(
[Parameter(Mandatory=$true)]
[array]$names,
[Parameter(Mandatory=$true)]
[array]$services,
[string]$path = (Get-ADDomain -Current LocalComputer).DistinguishedName
)

$EMPLACEMENT_FICHIER_LOGIN = "C:\Users\Administrator\Documents\logins.txt"

# Fonction pour ajouter un utilisateur
Function Ajout-Utilisateur
{
    param(
    [string]$service,
    [string]$path,
    [string]$entreprise
    )

    $givenname =       Read-Host "Quel est le prénom de l'utilisateur ? --------------------------------- "
    $surname =         Read-Host "Quel est le nom de famille de l'utilisateur ? ------------------------- "
    $samaccountname =  Read-Host "Quel est le login de l'utilisateur ? ---------------------------------- "
    $accountpassword = Read-Host "Quel est le mot de passe de l'utilisateur ? --------------------------- "
    $choix_role =      Read-Host "L'utilisateur est il un employé (1) ou un responsable (2) ? 1/2 : ----- "

    if ($choix_role -eq "1")
    {
        $role = "employes"
    }
    elseif ($choix_role -eq "2")
    {
        $role = "responsables"
    }
    New-ADUser -Name "$givenname $surname" -GivenName $givenname -Surname $surname -UserPrincipalName "$samaccountname@chasseneuil.tierslieux86.fr" -SamAccountName $samaccountname -Path "ou=$service,ou=$role,ou=utilisateurs,ou=$name,$path" -AccountPassword(ConvertTo-SecureString $accountpassword -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $false -PassThru | ForEach-Object {if ($choix_role -match "^[12]$") {Add-ADGroupMember -Identity "cn=$role-$service-$name,ou=groupes globaux,ou=$name,$path" -Members $_ -Verbose}}
    if (!(Test-Path -Path $script:EMPLACEMENT_FICHIER_LOGIN))
    {
        New-Item -Path $script:EMPLACEMENT_FICHIER_LOGIN -ItemType File
    }
    Add-Content -Path $script:EMPLACEMENT_FICHIER_LOGIN -Value "Login : $samaccountname Mot de passe : $accountpassword `n"

    $script:ajout_utilisateur = (Read-Host "Ajouter un nouvel utilisateur ? Oui/Non ") -match "^[oO][uU]?[iI]?$"
}

foreach ($name in $names)
{
    # On crée la structure basique pour chaque nom d'ou donné
    New-ADOrganizationalUnit -Name $name -Path $Path
    New-ADOrganizationalUnit -Name "utilisateurs" -Path "ou=$name,$path"
    New-ADOrganizationalUnit -Name "responsables" -Path "ou=utilisateurs,ou=$name,$path"
    New-ADOrganizationalUnit -Name "employes" -Path "ou=utilisateurs,ou=$name,$path"
    New-ADOrganizationalUnit -Name "ordinateurs" -Path "ou=$name,$path"
    New-ADOrganizationalUnit -Name "groupes globaux" -Path "ou=$name,$path"
    New-ADObject -Type "computer" -Name "ordinateur_test" -Path "ou=ordinateurs,ou=$name,$path"

    foreach ($service in $services)
    {
        # Si on a précisé des services, on crée leur arborescence et on utilise un fonction qui permet à l'utilisateur de rentrer les utilisateurs.
        if ($service -ne $null)
        {
            New-ADOrganizationalUnit -Name "$service" -Path "ou=employes,ou=utilisateurs,ou=$name,$path"
            New-ADOrganizationalUnit -Name "$service" -Path "ou=responsables,ou=utilisateurs,ou=$name,$path"
            New-ADGroup "employes-$service-$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global
            New-ADGroup "responsables-$service-$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global            
            $ajout_utilisateur = (Read-Host "Voulez-vous ajouter des utilisateurs au service $service de $name ? Oui/Non ") -match "^[oO][uU]?[iI]?$"
            while ($ajout_utilisateur)
            {
                Ajout-Utilisateur -service $service -entreprise $name -path $path
            }
        }
        # Sinon on crée l'arborescence commune
        else
        {
            New-ADGroup "employes_$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global
            New-ADGroup "responsables_$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global
            New-ADUser -Name "employe test" -GivenName "test" -Surname "employe" -UserPrincipalName "e_$name@chasseneuil.tierslieux86.fr" -SamAccountName "e_$name" -Path "ou=employes,ou=utilisateurs,ou=$name,$path" -AccountPassword(ConvertTo-SecureString "P@ssword1" -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $false -PassThru | ForEach-Object {Add-ADGroupMember -Identity "cn=employes_$name,ou=groupes globaux,ou=$name,$path" -Members $_ -Verbose}
            New-ADUser -Name "responsable test" -GivenName "test" -Surname "responsable" -UserPrincipalName "r_$name@chasseneuil.tierslieux86.fr" -SamAccountName "r_$name" -Path "ou=responsables,ou=utilisateurs,ou=$name,$path" -AccountPassword(ConvertTo-SecureString "P@ssword1" -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $false -PassThru | ForEach-Object {Add-ADGroupMember -Identity "cn=responsables_$name,ou=groupes globaux,ou=$name,$path" -Members $_ -Verbose}
        }
    }
}
